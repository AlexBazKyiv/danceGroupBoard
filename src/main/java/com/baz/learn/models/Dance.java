package com.baz.learn.models;

import javax.persistence.*;

@Entity
public class Dance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name, style, description, linkToInsert;

    @OneToOne(mappedBy = "dance", cascade = CascadeType.REFRESH)
    private ReportCard reportCard;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ReportCard getReportCard() {
        return reportCard;
    }

    public void setReportCard(ReportCard reportCard) {
        this.reportCard = reportCard;
    }

    public String getLinkToInsert() {
        return linkToInsert;
    }

    public void setLinkToInsert(String linkToInsert) {
        this.linkToInsert = linkToInsert;
    }
}
