package com.baz.learn.models;

import javax.persistence.*;

@Entity
public class ReportCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "dance_id")
    private Dance dance;

    private String userName;
    private String danceName;
    private int grades;

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName() {
        this.userName = user.getName();
    }

    public String getDanceName() {
        return danceName;
    }

    public void setDanceName() {
        this.danceName = dance.getName();
    }

    public void setGrades(int grades) {
        grades = grades;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Dance getDance() {
        return dance;
    }

    public void setDance(Dance dance) {
        this.dance = dance;
    }

    public Integer getGrades() {
        return grades;
    }

    public void setGrades(Integer grades) {
        grades = grades;
    }
}
