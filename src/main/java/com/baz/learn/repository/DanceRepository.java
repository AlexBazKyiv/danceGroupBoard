package com.baz.learn.repository;

import com.baz.learn.models.Dance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DanceRepository extends CrudRepository<Dance, Integer> {
}
