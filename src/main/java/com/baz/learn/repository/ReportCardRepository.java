package com.baz.learn.repository;

import com.baz.learn.models.ReportCard;
import org.springframework.data.repository.CrudRepository;

public interface ReportCardRepository extends CrudRepository<ReportCard, Integer> {
}
