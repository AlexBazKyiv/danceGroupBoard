package com.baz.learn.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("Baz")
                .password("0207")
                .roles("ADMIN")
                .and()
                .withUser("Galina")
                .password("041281")
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/user/report").hasAnyRole("USER", "ADMIN")
                    .antMatchers("/admin", "/admin/**").hasRole("ADMIN")
                .and()
                    .formLogin();
        http
                .csrf()
                .csrfTokenRepository(csrfTokenRepository())
                .ignoringAntMatchers("/admin/dance/add" , "/admin/user/add", "/admin/dance/edit/**", "/admin/user/edit/**");

        http.authorizeRequests().antMatchers(HttpMethod.POST).hasRole("ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.PUT).hasRole("ADMIN");
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {

        return new HttpSessionCsrfTokenRepository();
    }

    @Bean
    public PasswordEncoder encoder(){
        return NoOpPasswordEncoder.getInstance();
    }
}
