package com.baz.learn.controllers;

import com.baz.learn.models.User;
import com.baz.learn.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    @GetMapping("/admin")
    public String adminPage(Model model) {
        model.addAttribute("title", "Администрирование");
        return "admin/admin-main";
    }

    @GetMapping("/admin/user/add")
    public String addUserAdmin(Model model) {
        return "admin/admin-userAdd";
    }

    @GetMapping("/admin/dance/add")
    public String addDanceMain(Model model) {
        return "admin/admin-danceAdd";
    }

}
