package com.baz.learn.controllers;

import com.baz.learn.models.User;
import com.baz.learn.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public String userMain(Model model) {
        List<String> usersNames = userService.getFindAllNames();
        model.addAttribute("usersNames", usersNames);
        return "user-main";
    }
    @GetMapping("/admin/user")
    public String userPageViewAdmin(Model model) {
        Iterable<User> users = userService.getFindAll();
        model.addAttribute("users", users);
        return "admin/admin-user";
    }

    @PostMapping("/admin/user/add")
    public String addNewUserDoAdmin(@RequestParam String name, @RequestParam String phoneNumber,
                                    @RequestParam String email, @RequestParam String login,
                                    @RequestParam String password, Model model) {
        User user = new User(name, phoneNumber,email,login,password);
        userService.addNewUser(user);
        return "redirect:/admin/user";
    }

    @GetMapping("/admin/user/delete/yes/{id}")
    String deleteUserDoAdminAfterConfirmation(@PathVariable(value = "id") int id, Model model) {
        userService.deleteUser(id);
        return "redirect:/admin/user";
    }

    @GetMapping("/admin/user/delete/{id}")
    String deleteUserDoAdmin(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("user", userService.getFindById(id));
        return "admin/admin-userDelete";
    }

    @GetMapping("/admin/user/edit/{id}")
    String editDanceDoAdmin(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("user", userService.getFindById(id));
        return "admin/admin-userEdit";
    }

    @PostMapping("/admin/user/edit/{id}")
    String editDanceDoAdmin(@PathVariable(value = "id") int id, @RequestParam String name, @RequestParam String phoneNumber,
                            @RequestParam String email, @RequestParam String login,
                            @RequestParam String password, Model model) {
        User user = new User();
        userService.deleteUser(id);
        user.setId(id);
        user.setName(name);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        user.setLogin(login);
        user.setPassword(password);
        userService.addNewUser(user);
        return "redirect:/admin/user";
    }

}
