package com.baz.learn.controllers;

import com.baz.learn.models.Dance;
import com.baz.learn.service.DanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DanceController {
    @Autowired
    private DanceService danceService;

    @GetMapping("/dance")
    public String danceMain(Model model) {
        Iterable<Dance> dances = danceService.getAllDance();
        model.addAttribute("dances", dances);
        return "dance-main";
    }

    @GetMapping("/admin/dance")
    public String danceMainDoAdmin(Model model) {
        Iterable<Dance> dances = danceService.getAllDance();
        model.addAttribute("dances", dances);
        return "admin/admin-dance";
    }

    @PostMapping("/admin/dance/add")
    String addNewDanceDoAdmin(@RequestParam String name, @RequestParam String style,
                              @RequestParam String linkToInsert, @RequestParam String description,
                              Model model) {
        Dance dance = new Dance();
        dance.setName(name);
        dance.setStyle(style);
        dance.setLinkToInsert(linkToInsert);
        dance.setDescription(description);
        danceService.addNewDance(dance);
        return "redirect:/admin/dance";
    }

    @PostMapping("/admin/dance/edit/{id}")
    String editDanceDoAdmin(@PathVariable(value = "id") int id, @RequestParam String name,
                            @RequestParam String style, @RequestParam String linkToInsert,
                            @RequestParam String description, Model model) {
        Dance dance = new Dance();
        danceService.deleteDance(id);
        dance.setId(id);
        dance.setName(name);
        dance.setStyle(style);
        dance.setLinkToInsert(linkToInsert);
        dance.setDescription(description);
        danceService.addNewDance(dance);
        return "redirect:/admin/dance";
    }

    @GetMapping("/dance/{id}")
    public String danceDetails(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("dance", danceService.getFindById(id));
        return "danceDetails";
    }

    @GetMapping("/admin/dance/delete/yes/{id}")
    String deleteDanceDoAdminAfterConfirmation(@PathVariable(value = "id") int id, Model model) {
        danceService.deleteDance(id);
        return "redirect:/admin/dance";
    }

    @GetMapping("/admin/dance/delete/{id}")
    String deleteDanceDoAdmin(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("dance", danceService.getFindById(id));
        return "admin/admin-danceDelete";
    }

    @GetMapping("/admin/dance/edit/{id}")
    String editDanceDoAdmin(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("dance", danceService.getFindById(id));
        return "admin/admin-danceEdit";
    }

}
