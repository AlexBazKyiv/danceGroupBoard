package com.baz.learn.service;

import com.baz.learn.models.Dance;
import com.baz.learn.repository.DanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class DanceService {
    @Autowired
    private DanceRepository danceRepository;

    public void deleteDance(int id) {
        danceRepository.deleteById(id);
    }

    public void addNewDance (Dance dance) {
        danceRepository.save(dance);
    }

    public Dance getFindById (int id) {
        Optional<Dance> danceOptional = danceRepository.findById(id);
        ArrayList<Dance> danceArrayList = new ArrayList<>();
        danceOptional.ifPresent(danceArrayList::add);
        Dance dance = danceArrayList.get(0);
        return dance;
    }

    public Iterable<Dance> getAllDance () {
        return danceRepository.findAll();
    }
}
