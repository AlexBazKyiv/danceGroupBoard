package com.baz.learn.service;

import com.baz.learn.models.Dance;
import com.baz.learn.models.ReportCard;
import com.baz.learn.models.User;
import com.baz.learn.repository.ReportCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportCardService {
    @Autowired
    ReportCardRepository reportCardRepository;

    public void addNewReportCard(User user, Dance dance) {
        ReportCard reportCard = new ReportCard();
        reportCard.setDance(dance);
        reportCard.setUser(user);
        reportCardRepository.save(reportCard);
    }

    public String toGrade(String userName, String danceName, int grade) {
        String result = "Username or dance name entered incorrectly";
        Iterable<ReportCard> reportCards = reportCardRepository.findAll();
        for (ReportCard r : reportCards
        ) {
            if (r.getUserName().equals(userName) & r.getDanceName().equals(danceName)) {
                r.setGrades(grade);
                result = "Saved";
            }
        }
        return result;
    }
}
