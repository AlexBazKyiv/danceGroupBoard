package com.baz.learn.service;

import com.baz.learn.models.Dance;
import com.baz.learn.models.User;
import com.baz.learn.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<String> getFindAllNames() {
        List<String> namesUsers = new ArrayList<>();
        Iterable<User> users = getFindAll();
        for (User u:users) {
           namesUsers.add(u.getName());
        }
        return namesUsers;
    }

    public Iterable<User> getFindAll() {
        return userRepository.findAll();
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    public void addNewUser (User user) {
        userRepository.save(user);
    }

    public User getFindById (int id) {
        Optional<User> userOptional = userRepository.findById(id);
        ArrayList<User> userArrayList = new ArrayList<>();
        userOptional.ifPresent(userArrayList::add);
        User user = userArrayList.get(0);
        return user;
    }
    public Map<Integer, User> prepareForChangeUser (int id) {
        Map<Integer, User> profile = new LinkedHashMap<Integer, User>();
        profile.put(id, getFindById(id));
        return profile;
    }
}
