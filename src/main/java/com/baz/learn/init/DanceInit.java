package com.baz.learn.init;

import com.baz.learn.models.Dance;
import com.baz.learn.repository.DanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import static java.lang.System.lineSeparator;

@Component
public class DanceInit implements ApplicationRunner {
    private DanceRepository danceRepository;

    @Autowired
    public DanceInit(DanceRepository danceRepository) {
        this.danceRepository = danceRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = danceRepository.count();

        if (count == 0) {
            Dance d1 = new Dance();
            d1.setName("Балаган");
            d1.setStyle("Мизрахи");
            d1.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/sRUS3JZuz7A\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d1.setDescription("Перый наш танец в стиле Мизрахи. " +
                    "Назван по одноименному названию песни Сарит Хадат." +
                    " Хорошо подходит для изучения этого стиля и его базовых движений");

            Dance d2 = new Dance();
            d2.setName("Адама (на земле)");
            d2.setStyle("Израильский");
            d2.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Tph0a6vGAmM\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d2.setDescription("Создан в стиле Израильских круговых танцев." +
                    " Текст песни в переводе приблизительно такой " +
                    "- на земле, в воздухе, в воде, в том что я вижу, я ощущаю Господа");

            Dance d3 = new Dance();
            d3.setName("Самеях (Радость)");
            d3.setStyle("Мизрахи");
            d3.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Zq58rJH2dbw\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d3.setDescription("Слово Самеях на иврите обозначает и радость и танец одновременно." +
                    " В Писаниях эти два понятия очень взаимосвязанны." +
                    "Назван по одноименному названию песни Сарит Хадат." +
                    "В танце явно выраженно присутствуют движения стиля Дебка");

            Dance d4 = new Dance();
            d4.setName("Городской");
            d4.setStyle("Местечковый");
            d4.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/dS611O7a_-4\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d4.setDescription("Танец в местечковом стиле под музыку Варшавский Фрейлахс" +
                    "Довольно быстрый, с усложненными элементами основанными на классической хореографии." +
                    "Парный");

            Dance d5 = new Dance();
            d5.setName("Маим-Хаим ( Вода жизни )");
            d5.setStyle("Израильский");
            d5.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/_ZSyI5H35SU\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d5.setDescription("В танце использованны елементы как Мизрахи так и классической хореографии (контемпа)." +
                    "Танец женский, мужская часть связанна с волной из ткани. Используются бубны");

            Dance d6 = new Dance();
            d6.setName("Едит");
            d6.setStyle("Мизрахи");
            d6.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/yjUrvsuAJS8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d6.setDescription("Танец в стиле Мизрахи, один из самых сложных наших танцев в этом стиле." +
                    "Танец парный, но может быть разведен и на не равное число участников (но четное) ");

            Dance d7 = new Dance();
            d7.setName("Мимицраим Геалтейну (из Египта вывел)");
            d7.setStyle("Мизрахи");
            d7.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/rM64W-p13ew\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d7.setDescription("Танец посвященный выходу еврейского народа из Египта." +
                    "Может быть использованн различный инвентарь подчеркивающий историю как Исхода так и Шавуота." +
                    "Танец на малое количество участников - парный, если на большое количество то можно не парное (но четное).");

            Dance d8 = new Dance();
            d8.setName("Шалом Алейхем");
            d8.setStyle("Мизрахи");
            d8.setLinkToInsert("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/PJnfpXBbTY4\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
            d8.setDescription("Танец в стиле Мизрахи, может быть разведен в соотношении 2 к 1 (девочки и мальчики) или в другом варианте. " +
                    "Можно танцевать как на небольшое количество парБ так и массово." +
                    "На большое количество лучше видны рисунки(диогонали) ");


            danceRepository.save(d1);
            danceRepository.save(d2);
            danceRepository.save(d3);
            danceRepository.save(d4);
            danceRepository.save(d5);
            danceRepository.save(d6);
            danceRepository.save(d7);
            danceRepository.save(d8);

        }
    }
}
