package com.baz.learn.init;

import com.baz.learn.models.User;
import com.baz.learn.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class UserInit implements ApplicationRunner {
    private UserRepository userRepository;

    @Autowired
    public UserInit(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = userRepository.count();

        if (count == 0) {
            User u1 = new User();
            u1.setName("Sasha");
            u1.setPhoneNumber("2342534656");
            u1.setEmail("gfgd@jghjgh");
            u1.setLogin("Sasha");
            u1.setPassword("1234");

            User u2 = new User();
            u2.setName("Dasha");
            u2.setPhoneNumber("245342534656");
            u2.setEmail("tghj@jghjggh");
            u2.setLogin("Dasha");
            u2.setPassword("123456");

            userRepository.save(u1);
            userRepository.save(u2);
        }
    }
}
